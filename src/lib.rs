
pub trait Sorter {
    fn sort<T>(&self, slice: &mut [T]) where T: Ord;
}

mod bubble_sort;
mod insertion_sort;
mod selection_sort;
mod quick_sort;

pub use bubble_sort::BubbleSort;
pub use insertion_sort::InsertionSort;
pub use selection_sort::SelectionSort;
pub use quick_sort::QuickSort;
pub struct StdSorter;
impl Sorter for StdSorter {
    fn sort<T>(&self, slice: &mut [T]) where T: Ord {
        slice.sort();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn std_works() {
        let mut things = vec![4, 2, 3, 1];
        StdSorter.sort(&mut things);
        assert_eq!(things, &[1, 2, 3, 4]);
    }
}
