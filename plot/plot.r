#!/usr/bin/env Rscript
t <- read.table('values.dat', header=TRUE)
library(ggplot2)
ggplot(t, aes(n, comparisons, colour = algorithm)) + geom_point() + geom_smooth() + scale_y_log10() + scale_x_log10()
